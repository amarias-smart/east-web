# EAST

*Empowering Access and System Tracker* is web-based tool that tracks agents' accesses from different tools per brand and vendor.

The tool also accommodates Medalla updates "realtime", which is dumped after uploading/updating agent db.

The tool have a variety of dashboards and reports.

<br>

## [Backend API](https://github.com/enablingservices-dev/east-api)

<br>

### **STATUS:** Deployed

<br>

# Enhancements
- Additional report tab "*Training Tracker*", a simple upload view as is report
- Improve dashboard by combining User Access per LOB and per Access
- CRUD for User Profile in Settings Page
- CRUD for User Access in Settings Page

<br>

# Server Details
### [**Development**](http://10.122.9.115:5000/EAST/)

SERVER | USERNAME | PASSWORD | DB CATALOG
--- | --- | --- | ---
IR-DEVTESTSVR | sa-dev | d3velopers.c0llide | EAST

<br>

### [**Production**](http://10.122.8.101/EAST/)

SERVER | USERNAME | PASSWORD | DB CATALOG
--- | --- | --- | ---
ICONDBSVR | sa-EAST | $A-3@st2021 | EAST

<br>

# Troubleshooting
*User unable to login*
> Verify if user is already authorize to access the tool.
>
> Verify if LDAP is working.

<br>

*Adding Users*
> This tool has a built-in user modification; check settings page.
>
> If user doesn't exist, and if requested, simply add the user with their corresponding profile

<br>

*Profile and Access*
> Creating/Updating profile can be done within the tool as long as the user is an ADMIN and has access to the setings page
>
> As for the profile's accesses this isn't straignforward and is not yet available with in the tool to change. Devs must make changes directly from the database.

<br>

*UAM DB Template*
> Just download either PLDT or SMART file below the summary table.
>
> Remove the following columns:
> - the 1st Column (which should be a date)
> - Tenure

<br>

*Blanks in UAM DB tables*
> This is caused by fields that have **new lines** as values which causes the file to have new columns with empty values. Remove these **new lines** and try uploading again.

<br>

*Manually dump Medalla files*
> Just access the following api to dump in the path (\\\10.122.9.21\medalla$):
> - /api/dump/medalliapldt
> - /api/dump/medalliasmart
>
> Files dumped are based on the currently uploaded UAM DBs

<br>

*Reports*
> All or majority of the reports are upload and show raw

<br>

<br>
<br>


# Folder Structure
```
src
├── assets                            # app related assets
│   ├── bg
│   ├── logo
│   ├── others
│   │   ├── tools table
│   │   └── transaction flow
│   └── logo
├── boot
│   ├── axios.js                      # change origin as desired
│   └── notifier.js                   # notification module
├── components                        # components separated per pages
│   ├── knowledgebase
│   │   ├── toolsmapping
│   │   └── useraccesstools
│   ├── layout
│   ├── layout-knowledgebase
│   ├── layout-settings
│   ├── misc
│   ├── report
│   │   ├── esolve
│   │   ├── ftealloc
│   │   ├── irab
│   │   ├── leavers-daily
│   │   ├── leavers-monthly
│   │   ├── medallia
│   │   ├── nonusage
│   │   ├── onehub
│   │   └── password
│   └── user-access
│   │   ├── audit
│   │   ├── history
│   │   ├── per-lob
│   │   ├── per-tool
│   └── └── summary
├── css                               # global css
├── layouts                           # layout templates for pages
├── pages                             # pages of the app
│   ├── Errors                        # pages for error routings
│   ├── KnowledgeBase                 # pages for FYI
│   ├── Report                        # pages for all Reports
│   ├── Settings                      # pages for Settings (admin)
│   ├── UserAccess                    # pages for User Access related pages
│   └── Index.vue                     # login page
├── repository                        # backend api calls
├── router                            # routes and routing configurations
├── store                             # app data storages
└── └── data                          # global app data storage
```

<br>

# Powered by
- [Quasar (2.0)](https://quasar.dev/)
- [Vue](https://vuejs.org/)
- [Vue Router](https://router.vuejs.org/)
- [Axios](https://github.com/axios/axios)
- [DevExtreme](https://js.devexpress.com/Overview/Vue/)
- [ExcelJs](https://github.com/exceljs/exceljs)
- [File-Saver](https://github.com/eligrey/FileSaver.js)
- [File-SaverJs](https://github.com/clarketm/FileSaver.js)
- [Jsonata](https://jsonata.org/)
- [Jspdf](https://github.com/parallax/jsPDF)
- [Jspdf-AutoTable](https://github.com/simonbengtsson/jsPDF-AutoTable)
- [Lodash](https://lodash.com/)
- [Moment](https://momentjs.com/)
- [Papaparse](https://www.papaparse.com/)
- [Particles.js](https://github.com/VincentGarreau/particles.js/)
- [Tabulator](http://tabulator.info/)
- [V-Viewer](https://mirari.cc/v-viewer/)
- [Vue-PivotTable](https://github.com/Seungwoo321/vue-pivottable)
- [Vuex-PersistedState](https://github.com/robinvdvleuten/vuex-persistedstate)
- [XLSX](https://sheetjs.com/)

<br>

# Basic Commands
## Install the dependencies
```bash
yarn install
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn lint
# or
npm run lint
```

### Build the app for production
```bash
quasar build
```

<br>

# Contributors
AMArias (*owner*)

AUAustria (*co-owner*)
